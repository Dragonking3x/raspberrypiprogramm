package com.vgdragon

import org.apache.commons.io.FileUtils
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader


class StartProgram {

    val programStartLock = Object()
    var programmStrartet = false

    var programTypeLock = Object()
    var programType = 0

    fun start(){
        println(getProgrammType())
        var proframTypeTemp = 0
        synchronized(programStartLock){
            if(programmStrartet)
                return
            else
                programmStrartet = true
        }
        synchronized(programTypeLock){
            if(programType < 1)
                return

            proframTypeTemp = programType
        }

        when(proframTypeTemp){
            1 -> program01()
        }
        synchronized(programStartLock){
            programmStrartet = false
        }

        setProgrammType(0)
    }

    fun setProgrammType(int: Int){
        synchronized(programTypeLock){
            programType = int
        }
    }
    fun getProgrammType(): Int{
        synchronized(programTypeLock){
            return programType
        }
    }
    fun isProgrammStarted(): Boolean{
        synchronized(programStartLock){
            return programmStrartet
        }
    }

    ///////////////////////////////////////

    val fileParth01 = "/media/usb/1"
    val fileParth02 = "/media/usb/2"


    fun program01() {
        var usbA = false
        var usbB = false

        val runtime = Runtime.getRuntime()
        while (true) {
            val commands = arrayOf("ls", "-l", "/dev/disk/by-uuid/")
            val proc = runtime.exec(commands)

            val stdInput = BufferedReader(InputStreamReader(proc.inputStream))

            val stdError = BufferedReader(InputStreamReader(proc.errorStream))

            var outputString = ""
            var errorString = ""

            var s: String? = null
            while (stdInput.readLine().also { s = it } != null) {
                outputString += s
                println(s)
            }

            while (stdError.readLine().also { s = it } != null) {
                errorString += s
                println(s)
            }
            if (!errorString.isBlank()) {
                Thread.sleep(1000 * 1)
                continue
            }
            if(!outputString.contains("/sda1"))
                usbA = false

            if (outputString.contains("/sda1") && !usbA) {
                usbA = if(usbB){
                    println("sda1: 2")
                    connectUSB("sda1","2")
                } else {
                    println("sda1: 1")
                    connectUSB("sda1","1")
                }
            }

            if(!outputString.contains("/sdb1"))
                usbB = false

            if (outputString.contains("/sdb1") && !usbB) {
                usbB = if(usbA){
                    println("sdb1: 2")
                    connectUSB("sdb1","2")

                } else {
                    println("sdb1: 1")
                    connectUSB("sdb1","1")
                }
            }

            if(usbA && usbB) {
                copyUSB(File(fileParth01), File(fileParth02))
                println("Mounted DONE")
                Thread.sleep(1000 * 10)
                break
            }
            println("usbA")
            println(usbA)
            println("usbB")
            println(usbB)
            Thread.sleep(1000 * 1)
        }
        println("DONE")
    }

    fun connectUSB(usbName: String, usbFolder: String): Boolean{
        val runtime = Runtime.getRuntime()
        val commands = arrayOf("sudo", "mount", "/dev/$usbName", "/media/usb/$usbFolder", "-o", "uid=pi,gid=pi")
        val proc = runtime.exec(commands)

        val stdInput = BufferedReader(InputStreamReader(proc.inputStream))

        val stdError = BufferedReader(InputStreamReader(proc.errorStream))

        var outputString = ""
        var errorString = ""

        var s: String? = null
        println("Output:")
        while (stdInput.readLine().also { s = it } != null) {
            outputString += s
            println(s)
        }
        println("Error:")
        while (stdError.readLine().also { s = it } != null) {
            errorString += s
            println(s)
        }
        if(errorString.isBlank())
            return true

        println("return connect")
        return errorString.contains("The volume may be already mounted")
    }

    fun copyUSB(fileSrc: File, fileTest :File){

        val listFiles = fileSrc.listFiles()

        for(file in listFiles){
            val name = file.name
            val tempFile = File(fileTest.absolutePath + "/" + name)
            if(file.isDirectory){
                tempFile.mkdir()
                copyUSB(file, tempFile)
            } else {
                FileUtils.copyFile(file, tempFile)
            }
        }

    }

}