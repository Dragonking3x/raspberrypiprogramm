package com.vgdragon

import com.pi4j.io.gpio.GpioFactory
import com.pi4j.io.gpio.PinPullResistance
import com.pi4j.io.gpio.RaspiPin
import com.pi4j.io.gpio.event.GpioPinListenerDigital



    fun main(args: Array<String>) {

        testing() //todo
        return //todo

        println("Code Start")
        val startProgram = StartProgram()


        // create gpio controller
        val gpio = GpioFactory.getInstance()

        // provision gpio pin #02 as an input pin with its internal pull down resistor enabled
        val startButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_01, PinPullResistance.PULL_DOWN)
        // set shutdown state for this input pin
        startButton.setShutdownOptions(true)
        // create and register gpio pin listener
        startButton.addListener(GpioPinListenerDigital { event ->
            if(!startProgram.isProgrammStarted() && startProgram.getProgrammType() > 0) {
                println("Program Started")
                startProgram.start()
            }
        })

        val program01Button = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, PinPullResistance.PULL_DOWN)
        program01Button.setShutdownOptions(true)
        program01Button.addListener(GpioPinListenerDigital { event ->
            if(startProgram.getProgrammType() != 1) {
                startProgram.setProgrammType(1)
                println("Program 1")
            }
        })

        // keep program running until user aborts (CTRL-C)
        while (true) {
            Thread.sleep(500)
        }

    }

fun testing(){

    println("Code Start")
    val startProgram = StartProgram()

    startProgram.program01()

}



